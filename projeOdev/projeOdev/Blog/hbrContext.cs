﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using projeOdev.Models;

namespace projeOdev.Blog
{
    public class hbrContext : DbContext
    {
        public hbrContext()
            : base("hbrContext")
        {
        }
        public DbSet<Haberler> Haberler { get; set; }
        public DbSet<Duyurular> Duyurular { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}