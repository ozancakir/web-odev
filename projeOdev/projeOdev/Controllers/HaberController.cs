﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using projeOdev.Models;
using projeOdev.Blog;
using PagedList;

namespace projeOdev.Controllers
{
    public class HaberController : Controller
    {
        private hbrContext db = new hbrContext();

        // GET: /Haber/
        public ActionResult Index(string siralama, string currentFilter, string arama, int? page)
        {
            ViewBag.CurrentSort = siralama;
            ViewBag.NameSortParm = String.IsNullOrEmpty(siralama) ? "haber_azalan" : "";
            if (arama != null)
            {
                page = 1;
            }
            else { arama = currentFilter; }
            ViewBag.CurrentFilter = arama;
            var haberler = from s in db.Haberler
                           select s;
            if (!String.IsNullOrEmpty(arama))
            {
                haberler = haberler.Where(s => s.Hbaslik.ToUpper().Contains(arama.ToUpper()));
            }
            switch (siralama)
            {
                case "haber_azalan":
                    haberler = haberler.OrderByDescending(s => s.Hbaslik);
                    break;
                default:
                    haberler = haberler.OrderBy(s => s.Hbaslik);
                    break;
            }

            int pageSize = 2;
            int pageNumber = (page ?? 1);
            return View(haberler.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Haber/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haberler haberler = db.Haberler.Find(id);
            if (haberler == null)
            {
                return HttpNotFound();
            }
            return View(haberler);
        }

        // GET: /Haber/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Haber/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HaberlerID,Hbaslik,Hicerik")] Haberler haberler)
        {
            if (ModelState.IsValid)
            {
                db.Haberler.Add(haberler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(haberler);
        }

        // GET: /Haber/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haberler haberler = db.Haberler.Find(id);
            if (haberler == null)
            {
                return HttpNotFound();
            }
            return View(haberler);
        }

        // POST: /Haber/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HaberlerID,Hbaslik,Hicerik")] Haberler haberler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(haberler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(haberler);
        }

        // GET: /Haber/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haberler haberler = db.Haberler.Find(id);
            if (haberler == null)
            {
                return HttpNotFound();
            }
            return View(haberler);
        }

        // POST: /Haber/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Haberler haberler = db.Haberler.Find(id);
            db.Haberler.Remove(haberler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
