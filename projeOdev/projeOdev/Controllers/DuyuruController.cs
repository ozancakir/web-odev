﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using projeOdev.Models;
using projeOdev.Blog;
using PagedList;

namespace projeOdev.Controllers
{
    public class DuyuruController : Controller
    {
        private hbrContext db = new hbrContext();

        // GET: /Duyuru/
        public ActionResult Index(string siralama, string currentFilter, string arama, int? page)
        {
            ViewBag.CurrentSort = siralama;
            ViewBag.NameSortParm = String.IsNullOrEmpty(siralama) ? "duyuru_azalan" : "";
            if (arama != null)
            {
                page = 1;
            }
            else { arama = currentFilter; }
            ViewBag.CurrentFilter = arama;

            var duyurular = from s in db.Duyurular
                            select s;
            if (!String.IsNullOrEmpty(arama))
            {
                duyurular = duyurular.Where(s => s.Dbaslik.ToUpper().Contains(arama.ToUpper()));
            }
            switch (siralama)
            {
                case "duyuru_azalan":
                    duyurular = duyurular.OrderByDescending(s => s.Dbaslik);
                    break;
                default:
                    duyurular = duyurular.OrderBy(s => s.Dbaslik);
                    break;
            }

            int pageSize = 4;
            int pageNumber = (page ?? 1);
            return View(duyurular.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Duyuru/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyurular duyurular = db.Duyurular.Find(id);
            if (duyurular == null)
            {
                return HttpNotFound();
            }
            return View(duyurular);
        }

        // GET: /Duyuru/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Duyuru/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DuyurularID,Dbaslik,Dicerik")] Duyurular duyurular)
        {
            if (ModelState.IsValid)
            {
                db.Duyurular.Add(duyurular);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(duyurular);
        }

        // GET: /Duyuru/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyurular duyurular = db.Duyurular.Find(id);
            if (duyurular == null)
            {
                return HttpNotFound();
            }
            return View(duyurular);
        }

        // POST: /Duyuru/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DuyurularID,Dbaslik,Dicerik")] Duyurular duyurular)
        {
            if (ModelState.IsValid)
            {
                db.Entry(duyurular).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(duyurular);
        }

        // GET: /Duyuru/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyurular duyurular = db.Duyurular.Find(id);
            if (duyurular == null)
            {
                return HttpNotFound();
            }
            return View(duyurular);
        }

        // POST: /Duyuru/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Duyurular duyurular = db.Duyurular.Find(id);
            db.Duyurular.Remove(duyurular);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
