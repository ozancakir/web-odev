﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Resources;
namespace projeOdev.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult ChangeCulture(string lang) 
        {
            if(lang != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
            }
            HttpCookie cookie = new HttpCookie("Languege");
            cookie.Value = lang;
            Response.Cookies.Add(cookie);
            return View("Index");
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Haberler()
        {
            ViewBag.Message = "Haberler";

            return View();
        }
        public ActionResult Duyurular()
        {
            ViewBag.Message = "Duyurular";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hakkımızdaki Herşey";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "İletişim Bilgileri";

            return View();
        }
    }
}