﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(projeOdev.Startup))]
namespace projeOdev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
